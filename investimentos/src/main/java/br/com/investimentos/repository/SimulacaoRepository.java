package br.com.investimentos.repository;

import br.com.investimentos.models.SimulacaoEntity;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<SimulacaoEntity, Integer> {
}
