package br.com.investimentos.repository;

import br.com.investimentos.models.InvestimentoEntity;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<InvestimentoEntity, Integer> {
}
