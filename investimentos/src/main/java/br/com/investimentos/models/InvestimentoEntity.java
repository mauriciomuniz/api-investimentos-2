package br.com.investimentos.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "investimentos")
public class InvestimentoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String nome;

    private double rendimentoAoMes;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SimulacaoEntity> simulacaoEntities = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public List<SimulacaoEntity> getSimulacaoEntities() {
        return simulacaoEntities;
    }

    public void setSimulacaoEntities(List<SimulacaoEntity> simulacaoEntities) {
        this.simulacaoEntities = simulacaoEntities;
    }
}
