package br.com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;

@Entity
@Table(name = "simulacoes")
public class SimulacaoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int investimentoID;

    private String nomeInteressado;

    @Email
    private String email;

    @Digits(integer = 15, fraction = 2)
    private double valorAplicado;

    private int quantidadeMeses;

    @Digits(integer = 15, fraction = 2)
    private double montante;

    @ManyToOne
    private InvestimentoEntity investimentoEntity;

    public int getInvestimentoID() {
        return investimentoID;
    }

    public void setInvestimentoID(int investimentoID) {
        this.investimentoID = investimentoID;
    }

    public InvestimentoEntity getInvestimentoEntity() {
        return investimentoEntity;
    }

    public void setInvestimentoEntity(InvestimentoEntity investimentoEntity) {
        this.investimentoEntity = investimentoEntity;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
