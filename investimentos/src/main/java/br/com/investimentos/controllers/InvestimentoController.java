package br.com.investimentos.controllers;

import br.com.investimentos.dtos.EntradaInvestimento;
import br.com.investimentos.dtos.EntradaSimulacao;
import br.com.investimentos.dtos.SaidaInvestimento;
import br.com.investimentos.dtos.SaidaSimulacao;
import br.com.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<SaidaInvestimento> consultaOpcoesDeInvestimento() {
        return investimentoService.consultaInvestimentos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaInvestimento criaNovoInvestimento(@RequestBody @Valid EntradaInvestimento entradaInvestimento) {
        SaidaInvestimento saidaInvestimento = investimentoService.criarInvestimento(entradaInvestimento);

        return saidaInvestimento;
    }

    @PostMapping(value = "/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaSimulacao realizaSimulacaoDeInvestimento(@PathVariable(name = "id") int id, @RequestBody @Valid EntradaSimulacao entradaSimulacao) {
        SaidaSimulacao saidaSimulacao = investimentoService.simularInvestimento(entradaSimulacao, id);

        return saidaSimulacao;
    }
}