package br.com.investimentos.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

public class SaidaSimulacao {

    private double rendimentoPorMes;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER, pattern = "0.00")
    private double montante;

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}