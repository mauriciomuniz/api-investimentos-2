package br.com.investimentos.services;

import br.com.investimentos.dtos.EntradaInvestimento;
import br.com.investimentos.dtos.EntradaSimulacao;
import br.com.investimentos.dtos.SaidaInvestimento;
import br.com.investimentos.dtos.SaidaSimulacao;
import br.com.investimentos.models.InvestimentoEntity;
import br.com.investimentos.models.SimulacaoEntity;
import br.com.investimentos.repository.InvestimentoRepository;
import br.com.investimentos.repository.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public SaidaInvestimento criarInvestimento (EntradaInvestimento entradaInvestimento) {
        InvestimentoEntity investimentoEntity = new InvestimentoEntity();
        SaidaInvestimento saidaInvestimento = new SaidaInvestimento();

        investimentoEntity.setNome(entradaInvestimento.getNome());
        investimentoEntity.setRendimentoAoMes(entradaInvestimento.getRendimentoAoMes());

        investimentoEntity = investimentoRepository.save(investimentoEntity);

        saidaInvestimento.setId(investimentoEntity.getId());
        saidaInvestimento.setNome(investimentoEntity.getNome());
        saidaInvestimento.setRendimentoAoMes(investimentoEntity.getRendimentoAoMes());

        return saidaInvestimento;
    }

    public List<SaidaInvestimento> consultaInvestimentos () {
        List<SaidaInvestimento> saidaInvestimentos = new ArrayList<>();

        for (InvestimentoEntity investimento : investimentoRepository.findAll()) {
            SaidaInvestimento saidaInvestimento = new SaidaInvestimento();

            saidaInvestimento.setId(investimento.getId());
            saidaInvestimento.setNome(investimento.getNome());
            saidaInvestimento.setRendimentoAoMes(investimento.getRendimentoAoMes());

            saidaInvestimentos.add(saidaInvestimento);
        }
        return saidaInvestimentos;
    }

    public SaidaSimulacao simularInvestimento (EntradaSimulacao entradaSimulacao, int id) {
        SaidaSimulacao saidaSimulacao = new SaidaSimulacao();
        InvestimentoEntity investimentoEntity = new InvestimentoEntity();
        SimulacaoEntity simulacaoEntity = new SimulacaoEntity();

        Optional<InvestimentoEntity> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            investimentoEntity  = investimentoOptional.get();
        }else {
            throw new RuntimeException("Investimento não encontrado");
        }

        saidaSimulacao.setRendimentoPorMes(investimentoEntity.getRendimentoAoMes());
        saidaSimulacao.setMontante(entradaSimulacao.getValorAplicado() * Math.pow(1 + investimentoEntity.getRendimentoAoMes()/100, entradaSimulacao.getQuantidadeMeses()));

        BigDecimal bigDecimal = new BigDecimal(saidaSimulacao.getMontante());
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        saidaSimulacao.setMontante(bigDecimal.doubleValue());

        simulacaoEntity.setEmail(entradaSimulacao.getEmail());
        simulacaoEntity.setMontante(saidaSimulacao.getMontante());
        simulacaoEntity.setNomeInteressado(entradaSimulacao.getNomeInteressado());
        simulacaoEntity.setQuantidadeMeses(entradaSimulacao.getQuantidadeMeses());
        simulacaoEntity.setValorAplicado(entradaSimulacao.getValorAplicado());

        simulacaoRepository.save(simulacaoEntity);

        return saidaSimulacao;
    }

}
