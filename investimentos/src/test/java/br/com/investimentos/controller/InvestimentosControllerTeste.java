package br.com.investimentos.controller;

import br.com.investimentos.controllers.InvestimentoController;
import br.com.investimentos.dtos.EntradaInvestimento;
import br.com.investimentos.dtos.SaidaInvestimento;
import br.com.investimentos.models.InvestimentoEntity;
import br.com.investimentos.services.InvestimentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(InvestimentoController.class)
public class InvestimentosControllerTeste {

    @MockBean
    private InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    EntradaInvestimento entradaInvestimento;
    InvestimentoEntity investimentoEntity;
    SaidaInvestimento saidaInvestimento;
    List<SaidaInvestimento> saidaInvestimentos;

    @BeforeEach
    private void setUp() {
        this.entradaInvestimento=new EntradaInvestimento();
        entradaInvestimento.setNome("CDB 140% CDI");
        entradaInvestimento.setRendimentoAoMes(2.0);

        this.investimentoEntity = new InvestimentoEntity();
        this.saidaInvestimento = new SaidaInvestimento();

        this.saidaInvestimentos= new ArrayList<>();
        saidaInvestimento.setNome("CDB 140% CDI");
        saidaInvestimentos.add(saidaInvestimento);

    }

    @Test
    public void testarCriarInvestimento() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(EntradaInvestimento.class)))
                .thenReturn(Mockito.any(SaidaInvestimento.class));
        ObjectMapper objectMapper = new ObjectMapper();

        String investimentoJson = objectMapper.writeValueAsString(entradaInvestimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON).content(investimentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarConsultarInvestimentos() throws Exception{
        Mockito.when(investimentoService.consultaInvestimentos()).thenReturn(saidaInvestimentos);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
}
