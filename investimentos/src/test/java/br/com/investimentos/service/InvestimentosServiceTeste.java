package br.com.investimentos.service;

import br.com.investimentos.dtos.EntradaInvestimento;
import br.com.investimentos.dtos.SaidaInvestimento;
import br.com.investimentos.models.InvestimentoEntity;
import br.com.investimentos.repository.InvestimentoRepository;
import br.com.investimentos.services.InvestimentoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class InvestimentosServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    EntradaInvestimento entradaInvestimento;
    SaidaInvestimento saidaInvestimento;
    List<SaidaInvestimento> saidaInvestimentos;
    InvestimentoEntity investimentoEntity;

    @BeforeEach
    private void setUp() {
        this.entradaInvestimento=new EntradaInvestimento();
        entradaInvestimento.setNome("CDB 140% CDI");
        entradaInvestimento.setRendimentoAoMes(2.0);

        this.saidaInvestimento = new SaidaInvestimento();

        this.saidaInvestimentos= new ArrayList<>();
        saidaInvestimentos.add(saidaInvestimento);

    }

    @Test
    public void testarCriarInvestimento() {
        Mockito.when(investimentoRepository.save(Mockito.any(InvestimentoEntity.class))).then(objeto -> objeto.getArgument(0));
    }

    @Test
    public void testarConsultarInvestimentos() {
        Mockito.when(investimentoRepository.findAll()).thenReturn((Iterable<InvestimentoEntity>) investimentoEntity);
    }
}
